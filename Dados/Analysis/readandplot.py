import cassiopeia as cass
from matplotlib import pyplot as plt
import json
from scipy import stats as st
import numpy as np


def percentages(array):
    for x in array: array[x] = array[x] / np.sum(array)
    return array


def getAttributes(file):
    '''
    :param file: 'str' arquivo onde está os dados
    :return: 'dict' Retorna três dicionários dos stats do Blue side, Red side e as Diferenças entre Blue - Red.
    Não deve ser utilizado livremente, utilizado puramente para analises e se for utilizado deve ser checado para ver se
    realmente está fazendo o que você quer.
    '''
    with open(file, 'r') as fp:
        data = json.load(fp)

    blueteamgold = []
    redteamgold = []
    blueteamdmg = []
    redteamdmg = []
    blueteamdmgc = []
    redteamdmgc = []
    bdrags = []
    bbarons = []
    bheralds = []
    rdrags = []
    rbarons = []
    rheralds = []
    btowers = []
    rtowers = []
    bblood = []
    rblood = []
    durations = []
    bwins = []
    rwins = []
    bkdamean = []
    rkdamean = []
    bkdasum = []
    rkdasum = []
    bpgoldmean = []
    rpgoldmean = []
    bgoldpercentages = []
    rgoldpercentages = []
    bdamagemean = []
    rdamagemean = []
    bdamagepercentages = []
    rdamagepercentages = []
    for i in range(len(data)):
        try:
            blueteamgold += [data[i]['teamgold'][0]]
            redteamgold += [data[i]['teamgold'][1]]
            blueteamdmg += [data[i]['teamdamage'][0]]
            redteamdmg += [data[i]['teamdamage'][1]]
            blueteamdmgc += [data[i]['teamdamagechamp'][0]]
            redteamdmgc += [data[i]['teamdamagechamp'][1]]
            bdrags += [data[i]['objetivos'][0]['dragon']]
            rdrags += [data[i]['objetivos'][1]['dragon']]
            bbarons += [data[i]['objetivos'][0]['baron']]
            rbarons += [data[i]['objetivos'][1]['baron']]
            bheralds += [data[i]['objetivos'][0]['herald']]
            rheralds += [data[i]['objetivos'][1]['herald']]
            btowers += [data[i]['towers'][0]]
            rtowers += [data[i]['towers'][1]]
            bblood += [data[i]['firstblood'][0]]
            rblood += [data[i]['firstblood'][1]]
            durations += [data[i]['duration'][0]]
            bwins += [data[i]['win'][0]]
            rwins += [data[i]['win'][1]]
            bkdamean += [np.mean(data[i]['kda'][0])]
            rkdamean += [np.mean(data[i]['kda'][1])]
            bkdasum += [np.sum(data[i]['kda'][0])]
            rkdasum += [np.sum(data[i]['kda'][1])]
            bpgoldmean += [np.mean(data[i]['playergold'][0])]
            rpgoldmean += [np.mean(data[i]['playergold'][1])]
            bgoldpercentages += [np.mean(percentages(data[i]['playergold'][0]))]
            rgoldpercentages += [np.mean(percentages(data[i]['playergold'][1]))]
            bdamagemean += [np.mean(data[i]['playerdamage'][0])]
            rdamagemean += [np.mean(data[i]['playerdamage'][1])]
            bdamagepercentages += [np.mean(percentages(data[i]['playerdamage'][0]))]
            rdamagepercentages += [np.mean(percentages(data[i]['playerdamage'][1]))]
        except:
            continue

    # differences from blue perspective
    teamgolddiff = np.subtract(blueteamgold, redteamgold)sum2
    teamdmgdiff = np.subtract(blueteamdmg, redteamdmg)
    teamdmgcdiff = np.subtract(blueteamdmgc, redteamdmgc)
    dragdiff = np.subtract(bdrags, rdrags)
    barondiff = np.subtract(bbarons, rbarons)
    heralddiff = np.subtract(bheralds, rheralds)
    towerdiff = np.subtract(btowers, rtowers)

    # correlation table:
    blue = {'BlueTeamGold': blueteamgold, 'BlueTeamDMG': blueteamdmg, 'BlueTeamDmgC': blueteamdmgc,
            'BlueDragons': bdrags,
            'BlueBarons': bbarons, 'BlueHeralds': bheralds, 'BlueTowers': btowers, 'BlueFB': bblood,
            'Duration': durations, 'BlueWins': bwins}
    red = {'RedTeamGold': redteamgold, 'RedTeamDMG': redteamdmg, 'RedTeamDmgC': redteamdmgc, 'RedDragons': rdrags,
           'RedBarons': rbarons, 'RedHeralds': rheralds, 'RedTowers': rtowers, 'RedFB': rblood, 'Duration': durations,
           'RedWins': rwins}
    diffs = {'GoldDiff': teamgolddiff, 'DmgDiff': teamdmgdiff, 'DmgCDiff': teamdmgcdiff, 'DragDiff': dragdiff,
             'BaronDiff': barondiff, 'HeraldDiff': heralddiff, 'TowerDiff': towerdiff}
    return blue, red, diffs


# correlations
def getCorrelation(x, y):
    '''
    :param x: 'vect' Um vetor qualquer x de mesmo tamanho que y
    :param y: 'vect' Um vetor qualquer y de mesmo tamanho que x
    :return: 'tuple' Com correlação de Spearman e de Pearson, nessa ordem, entre os dois vetores mandados
    '''
    return st.stats.spearmanr(x, y), np.corrcoef(x, y)


def getCorrMatrix(x, y):
    '''
    :param x: 'vect' Um vetor de vetores qualquer x
    :param y: 'vect' Um vetor de vetores qualquer y
    :return: 'tuple' de strings formatadas como tabela, formadas pelas correlações entre cada um dos xi e yi. A primeira
    informação da tuple é a correlação de Pearson, e a segunda de Spearman.
    '''
    matriz = []
    matriz2 = []
    for i in range(len(x + 1)):
        matriz += [[]]
        matriz2 += [[]]
        for c in range(len(y + 1)):
            matriz[i] += [0]
            matriz2[i] += [0]
    # redxred
    # spearmen
    for i in range(1, 11):
        for c in range(1, 11):
            matriz[i][c] = st.stats.spearmanr(x[i - 1], y[c - 1])[0]
            matriz2[i][c] = np.corrcoef(x[i - 1], y[c - 1])[1][0]

    stringpearson = ''
    stringspearman = ''
    for i in range(len(matriz)):
        stringlinha = str(matriz[i][0])
        stringlinha1 = str(matriz2[i][0])
        for j in range(1, len(matriz[0])):
            stringlinha += ', ' + str(matriz[i][j])
        for j in range(1, len(matriz2[0])):
            stringlinha1 += ', ' + str(matriz2[i][j])
        stringspearman += stringlinha + '\n'
        stringpearson += stringlinha1 + '\n'
    return stringpearson, stringspearman


def plot(x, y, labelx, labely, folder):
    '''
    :param x: 'vect' Um vetor qualquer x
    :param y: 'vect' Um vetor qualquer y
    :param labelx: 'str' O nome do vetor x
    :param labely: 'str' O nome do vetor y
    :param folder: 'str' O nome da pasta que deseja que o arquivo seja salvo.
    :return: Nada. Plota o gráfico e salva como arquivo dentro da pasta mandada.
    '''
    plt.figure()
    plt.plot(x, y, 'ro')

    # axis labeling
    plt.xlabel(labelx)
    plt.ylabel(labely)

    # figure name
    plt.title(labelx + ':' + labely)
    plt.savefig('./plots/' + folder + labelx + 'x' + labely + '.png')
    plt.close


blue, red, diff = getAttributes('./second batch/datamatches.json')
# folder as in 'plot/'
for keyblue in blue:
    for keyred in red:
        plot(blue[keyblue], red[keyred], keyblue, keyred, 'blueXred/')

for key1 in blue:
    for key2 in blue:
        plot(blue[key1], blue[key2], key1, key2, 'blueXblue/')

for key1 in red:
    for key2 in red:
        plot(red[key1], red[key2], key1, key2, 'redXred/')

for key1 in red:
    for key2 in diff:
        plot(red[key1], diff[key2], key1, key2, 'redXdiff/')

for key1 in blue:
    for key2 in diff:
        plot(blue[key1], diff[key2], key1, key2, 'blueXdiff/')

for key1 in diff:
    for key2 in diff:
        plot(diff[key1], diff[key2], key1, key2, 'diffXdiff/')
