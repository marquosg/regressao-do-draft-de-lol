import cassiopeia as cass
import json
import numpy as np
from pullData import *
import time, urllib3


def getPlayerMatches(keyapi, file='players.json', filef='matches.json'):
    '''
    :param keyapi: 'str' Chave de acesso para o API da riot
    :param file: 'str' Arquivo JSON tipo dicionário que contém os IDs dos jogadores e
    :param filef: 'str' Arquivo JSON para salvar o resultado
    :return: Nada.

    A função serve para pegar o ID da primeira partida do histórico de cada player, formatar para dicionario como
    ID: server e salvar no arquivo matches
    '''
    cass.set_riot_api_key(keyapi)

    with open(file, 'r') as fp:
        players = json.load(fp)

    matches = {}
    i = 1
    for key in players:
        try:
            matchesID = getChampHis(key, keyapi, ID=True, server=players[key], index0=0, index1=1)
            matches[matchesID[0]] = players[key]
            time.sleep(2)
            print(i)
            i += 1

        except cass.datastores.riotapi.common.APIRequestError:
            print('API Error')
            keyapi = str(input("Insert new API key:"))
            cass.set_riot_api_key(keyapi)

        except:
            print('Was not an API error')

        finally:
            try:
                matchesID = getChampHis(key, keyapi, ID=True, server=players[key], index0=0, index1=1)
                matches[matchesID[0]] = players[key]
                time.sleep(2)
                print(i)
                i += 1
            except Exception as e:
                print(e)
                print("Got an error :(. Imma keep going tho")
                print(i)
                i += 1
                continue

    with open(filef, 'w') as fp:
        json.dump(matches, fp)


def getMatchesStats(keyapi, file='matches.json', filef='matchesdatacomplete56k.json'):
    '''
    Intuito dessa função é pegar as estatísticas partindo de uma lista de partidas. As opções de estatísticas devem ser
    editadas manualmente... talvez seja melhor implementar como entrada?
    :param file: 'str' Arquivo tipo JSON tipo dicionário que contém os IDs e os servers das partidas. Deve estar
    formatado como ID: server. Deve conter .json no final.
    :param filef: 'str' Arquivo tipo JSON de saida dos dados. Deve conter .json no final.
    :param keyapi: Chave de acesso ao API da riot.
    :return: Nada
    '''
    cass.set_riot_api_key(keyapi)

    with open(file, 'r') as fp:
        matches = json.load(fp)
    # stats = {1: 'kda', 2: 'playergold', 3: 'teamgold', 4: 'playerdamagechamp', 5: 'teamdamagechamp',
    # 6: 'playerdamage', 7: 'teamdamage', 8: 'objetivos', 9: 'win', 10: 'towers', 11: 'duration', 12: 'champions',
    # 13: 'players', 14: 'firstblood'} stats completos
    stats = {1: 'kda', 2: 'playergold', 3: 'teamgold', 4: 'skip', 5: 'skip', 6: 'playerdamage',
             7: 'teamdamage', 8: 'objetivos', 9: 'win', 10: 'towers', 11: 'duration', 12: 'champions', 13: 'players',
             14: 'firstblood', 15: 'skip'}

    vectdata = []
    for key in matches:
        matchdict = {}
        server = matches[key]
        matchdict['server'] = server
        for a in range(1, len(stats) + 1):
            try:
                matchdict[stats[a]] = getMatchStat(key, keyapi, stats[a], server=server)
            except 401:
                print("Tried one time")
                try:
                    matchdict[stats[a]] = getMatchStat(key, keyapi, stats[a], server=server)
                except:
                    print("Had to continue...")
                    continue
        vectdata += [matchdict]

    with open(filef, 'w') as fp:
        json.dump(vectdata, fp)


def getPlayerMatchesStats(keyapi, filep='players.json', filem='matches.json', files='matchestats.json'):
    '''
    O intuito dessa função é diminuir o tempo total do algoritmo. Em um numero elevado de players, o antigo estava
    demorando aproximadamente 2,4 dias para completar. Apenas para puxar as partidas. Essa função tenta fazer tudo
    em um só acesso ao API da riot para diminuir o tempo total de usar as duas funções, mesmo que o tempo para iterar
    seja o mesmo...
    :param keyapi: 'str' Chave de acesso ao API da riot.
    :param filep:'str' Arquivo tipo JSON para acessar os IDs dos usuários.
    :param filem: 'str' Arquivo tipo JSON para salvar os IDs das partidas.
    :param files: 'str' Arquivo tipo JSON para salvar as estatísticas das partidas.
    :return: Nada.
    '''
    start = time.time()

    with open(filep, 'r') as fp:
        players = json.load(fp)

    try:
        with open(filem, 'r') as fp:
            matches = json.load(fp)
    except:
        matches = {}
    try:
        with open(files, 'r') as fp:
            liststats = json.load(fp)
    except:
        liststats = []

    playersduple = list(players.items())
    print(playersduple[0])
    results = list(map(fatStats, playersduple))
    print(results)
    for i in range(len(results)):
        if results[i] is not None:
            matches.update(results[i][0])
            liststats.append(results[i][1])

    with open(filem, 'w') as fp:
        json.dump(matches, fp)

    with open(files, 'w') as fp:
        json.dump(liststats, fp)

    end = time.time()
    print("Finished. Took %s seconds" % (end - start))


keyapi = 'RGAPI-a4ce06e5-3566-40ac-9af0-b71828105625'
cass.set_riot_api_key(keyapi)
http = urllib3.PoolManager(retries=False)
getPlayerMatchesStats(keyapi, filep='players13.json')
getPlayerMatchesStats(keyapi, filep='players14.json')