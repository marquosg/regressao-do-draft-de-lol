import cassiopeia as cass
import json
import numpy as np
from pullData import *
import time


def addPlayersFromMatch(keyapi, file='players.json'):
    '''
    :param keyapi: 'str' Chave de acesso ao API da riot
    :param file: 'str' Arquivo que deseja acessar para pegar os jogadores.
    Deve estar formatada com um dict [player]=server.
    :return: Nada. Atualiza o arquivo adicionando mais jogadores ao database que já foi adicionado.
    '''
    cass.set_riot_api_key(keyapi)
    with open(file, 'r') as fp:
        dict = json.load(fp)

    newdict = dict.copy()
    for key in dict:
        try:
            match = getChampHis(str(key), keyapi, ID=True, server=dict[key], index0=0, index1=3)
            for i in range(len(match)):
                players = getMatchStat(match[i], keyapi, 'players', server=dict[key])
                for k in range(len(players)):
                    newdict[players[k]] = dict[key]
        except:
            print("Tried. Now waiting...")
            time.sleep(60)
            try:
                print("Trying one last time...")
                match = getChampHis(str(key), keyapi, ID=True, server=dict[key], index0=0, index1=3)
                for i in range(len(match)):
                    players = getMatchStat(match[i], keyapi, 'players', server=dict[key])
                    for k in range(len(players)):
                        newdict[players[k]] = dict[key]
            except:
                print("Gave up.")
                continue

    with open(file, 'w') as fp:
        json.dump(newdict, fp)


def getPlayersFromJson(key):
    '''
    :param key: 'str' Chave de acesso ao API da riot
    :return: Nada
    Foi uma função que eu fiz só para atualizar para um modelo diferente de salvar os dados. Apenas tratamento.
    Não usar.
    '''
    with open('players.json', 'r') as fp:
        dict = json.load(fp)

    with open('data.json', 'r') as fp:
        data = json.load(fp)

    for i in range(len(data)):
        players = data[i]['players']
        for j in range(len(players)):
            dict[players[j]] = data[i]['server']

    with open('players.json', 'w') as fp:
        json.dump(dict, fp)


def getElos(keyapi, filein, fileout):
    '''
    :param keyapi: 'str' Chave de acesso ao API da riot
    :param filein: 'str' Arquivo JSON dos jogadores que deve se pegar os elos. Deve estar como 'players.json'
    :param fileout 'str' Arquivo JSON que deve ficar salvo o elos dos jogadores. Deve estar como 'elo.json'
    :return: Nada
    Uma função que eu fiz para poder visualizar os elos da coletania.
    '''
    with open(filein, 'r') as fp:
        dict = json.load(fp)

    for key in dict:
        elos = getPlayerStats(key, dict[key], keyapi, "elo")
        print(elos)
        if len(elos) == 2:
            dadosdict = {'region': dict[key], 'solo': elos[0], 'flex': elos[1]}
        elif len(elos) == 1:
            dadosdict = {'region': dict[key], 'solo': elos[0], 'flex': None}
        else:
            dadosdict = {'region': dict[key], 'solo': None, 'flex': None}
        dict[key] = dadosdict

    with open(fileout, 'w') as fp:
        json.dump(dict, fp)


keyapi = 'RGAPI-0eeef689-6d01-4658-92a6-0c34388fda9f'
addPlayersFromMatch(keyapi)