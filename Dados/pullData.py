import cassiopeia as cass
import matplotlib
from cassiopeia import Summoner
from itertools import repeat
import time
from urllib3.exceptions import NewConnectionError


def getchamp(id):
    '''
    :param id: recebe o ID de um campeão
    :return: Nome do campeão em português
    '''
    champs = cass.get_champions('BR')
    for y in range(len(champs)):
        if champs[y].id == id:
            return champs[y].name
    return


def fatStats(duple):
    '''
    :param duple: 'duple' Recebe um duple com posição 0 o ID de um jogador e posição 1 o servidor em questão
    :return: 'duple' Retorna uma dupla com posição 0 um 'dict' contendo o ID da primeira partida do histórico
    no formato {matchID: server} e um dicionário de estatísticas dessa partida.
    '''
    ID = duple[0]
    server = duple[1]
    try:
        matchesID = getChampHis(ID, ID=True, server=server, index0=0, index1=1)
        matchID, match = {matchesID[0]: server}, cass.get_match(matchesID[0], region=server)
        stats = {'kda': 1, 'playergold': 2, 'teamgold': 3,
                 'playerdamagechamp': 4, 'teamdamagechamp': 5, 'playerdamage': 6, 'teamdamage': 7, 'objetivos': 8,
                 'win': 9,
                 'towers': 10, 'duration': 11, 'champions': 12, 'players': 13, 'firstblood': 14}

        estatis = list(map(getMatchStat, stats, repeat(match, len(stats))))
        estatist = {}
        list(map(estatist.update, estatis))
        print(matchID, estatist)
        return matchID, estatist
    except cass.datastores.riotapi.common.APIRequestError:
        print('API Error')
        keyapi = str(input("Insert new API key:"))
        cass.set_riot_api_key(keyapi)
        try:
            matchesID = getChampHis(ID, ID=True, server=server, index0=0, index1=1)
            matchID, match = {matchesID[0]: server}, cass.get_match(matchesID[0], region=server)
            stats = {'kda': 1, 'playergold': 2, 'teamgold': 3,
                     'playerdamagechamp': 4, 'teamdamagechamp': 5, 'playerdamage': 6, 'teamdamage': 7, 'objetivos': 8,
                     'win': 9,
                     'towers': 10, 'duration': 11, 'champions': 12, 'players': 13, 'firstblood': 14}

            estatis = list(map(getMatchStat, stats, repeat(match, len(stats))))
            estatist = {}
            list(map(estatist.update, estatis))
            print(matchID, estatist)
            return matchID, estatist
        except Exception as e:
            print("oops got and error")
            print(e)
            print("imma try again")
            if e == ConnectionError or NewConnectionError:
                print('flws')
                return
            try:
                matchesID = getChampHis(ID, ID=True, server=server, index0=0, index1=1)
                matchID, match = {matchesID[0]: server}, cass.get_match(matchesID[0], region=server)
                stats = {'kda': 1, 'playergold': 2, 'teamgold': 3,
                         'playerdamagechamp': 4, 'teamdamagechamp': 5, 'playerdamage': 6, 'teamdamage': 7,
                         'objetivos': 8,
                         'win': 9,
                         'towers': 10, 'duration': 11, 'champions': 12, 'players': 13, 'firstblood': 14}

                estatis = list(map(getMatchStat, stats, repeat(match, len(stats))))
                estatist = {}
                list(map(estatist.update, estatis))
                print(matchID, estatist)
                return matchID, estatist
            except Exception as e:
                print("oops got and error ")
                print(e)
                print("im finished tho")
                return
    except Exception as e:
        print("oops got and error")
        print(e)
        if e == ConnectionError or NewConnectionError:
            print('flws')
            return
        print("imma try again")
        try:
            matchesID = getChampHis(ID, ID=True, server=server, index0=0, index1=1)
            matchID, match = {matchesID[0]: server}, cass.get_match(matchesID[0], region=server)
            stats = {'kda': 1, 'playergold': 2, 'teamgold': 3,
                     'playerdamagechamp': 4, 'teamdamagechamp': 5, 'playerdamage': 6, 'teamdamage': 7, 'objetivos': 8,
                     'win': 9,
                     'towers': 10, 'duration': 11, 'champions': 12, 'players': 13, 'firstblood': 14}

            estatis = list(map(getMatchStat, stats, repeat(match, len(stats))))
            estatist = {}
            list(map(estatist.update, estatis))
            print(matchID, estatist)
            return matchID, estatist
        except Exception as e:
            print("oops got and error ")
            print(e)
            print("im finished tho")
            return

def getChampHis(summ, ID=False, server='BR', index0=0, index1=20):
    '''
    :param summ: 'str' o ID de um perfil ou o username
    :param key: 'str' a chave de acesso ao API da riot
    :param ID: 'bool' True se o summ for o ID False se for o username
    :param server: 'str' Servidor do perfiç
    :param index0: 'int' Começo do intervalo dos jogos ex:0 começa a pegar desde o primeiro jogo
    :param index1: 'int' Fim do inverfalo dos jogos ex:4 pega até o terceiro jogo
    :return: 'list' lista com os IDs dos jogos
    '''

    if not ID:
        summoner = cass.get_summoner(name=summ, region=server)
    else:
        summoner = cass.get_summoner(id=summ, region=server)

    jogos = cass.get_match_history(summoner, index0, index1)
    jogosid = []
    for i in range(len(jogos)):
        jogosid += [jogos[i].id]
    return jogosid


def getMatchStat(stat, jogo):
    '''
    :param match: 'str' ID da partida
    :param key: 'str' Chave do API da riot
    :param stat: 'str' Nome da estatística que seja puxar, recomendado iterar por um dicionario. Opções são:
        {'kda': 1,'playergold': 2, 'teamgold': 3,
         'playerdamagechamp': 4, 'teamdamagechamp': 5, 'playerdamage': 6, 'teamdamage': 7, 'objetivos': 8, 'win': 9,
         'towers': 10, 'duration': 11, 'champions': 12, 'players': 13, 'firstblood': 14, 'skip': 15}
         ou

    :param server: 'str' servidor da partida
    :return: 'vect' Vetor com a estatística desejada. Se aplicavel, o lado azul estará no [0] e o vermelho no [1]
    '''

    if stat == 'kda':
        blueteam = []
        for i in range(5):
            data = jogo.blue_team.participants[i].stats.kda
            blueteam += [data]
        redteam = []
        for i in range(5):
            data = jogo.red_team.participants[i].stats.kda
            redteam += [data]
        return {stat: [blueteam] + [redteam]}

    elif stat == 'playergold':
        blueteam = []
        for i in range(5):
            data = jogo.blue_team.participants[i].stats.gold_earned
            blueteam += [data]
        redteam = []
        for i in range(5):
            data = jogo.red_team.participants[i].stats.gold_earned
            redteam += [data]
        return {stat: [blueteam] + [redteam]}

    elif stat == 'teamgold':
        blueteam = 0
        for i in range(5):
            data = jogo.blue_team.participants[i].stats.gold_earned
            blueteam += data
        redteam = 0
        for i in range(5):
            data = jogo.red_team.participants[i].stats.gold_earned
            redteam += data
        return {stat: [blueteam] + [redteam]}

    elif stat == 'playerdamagechamp':
        blueteam = []
        for i in range(5):
            data = jogo.blue_team.participants[i].stats.total_damage_dealt_to_champions
            blueteam += [data]
        redteam = []
        for i in range(5):
            data = jogo.red_team.participants[i].stats.total_damage_dealt_to_champions
            redteam += [data]
        return {stat: [blueteam] + [redteam]}

    elif stat == 'teamdamagechamp':
        blueteam = 0
        for i in range(5):
            data = jogo.blue_team.participants[i].stats.total_damage_dealt_to_champions
            blueteam += data
        redteam = 0
        for i in range(5):
            data = jogo.red_team.participants[i].stats.total_damage_dealt_to_champions
            redteam += data
        return {stat: [blueteam] + [redteam]}
    elif stat == 'playerdamage':
        blueteam = []
        for i in range(5):
            data = jogo.blue_team.participants[i].stats.total_damage_dealt
            blueteam += [data]
        redteam = []
        for i in range(5):
            data = jogo.red_team.participants[i].stats.total_damage_dealt
            redteam += [data]
        return {stat: [blueteam] + [redteam]}
    elif stat == 'teamdamage':
        blueteam = 0
        for i in range(5):
            data = jogo.blue_team.participants[i].stats.total_damage_dealt
            blueteam += data
        redteam = 0
        for i in range(5):
            data = jogo.red_team.participants[i].stats.total_damage_dealt
            redteam += data
        return {stat: [blueteam] + [redteam]}
    elif stat == 'objetivos':
        blueteam = {'dragon': jogo.blue_team.dragon_kills, 'baron': jogo.blue_team.baron_kills,
                    'herald': jogo.blue_team.rift_herald_kills}
        redteam = {'dragon': jogo.red_team.dragon_kills, 'baron': jogo.red_team.baron_kills,
                   'herald': jogo.red_team.rift_herald_kills}
        return {stat: [blueteam] + [redteam]}
    elif stat == 'win':
        if jogo.red_team.win:
            return {stat: [0, 1]}
        else:
            return {stat: [1, 0]}
    elif stat == 'towers':
        return {stat: [jogo.blue_team.tower_kills, jogo.red_team.tower_kills]}
    elif stat == 'duration':
        return {stat: [jogo.duration.seconds]}
    elif stat == 'champions':
        blueteam = []
        for i in range(5):
            id = jogo.blue_team.participants[i].champion.id
            name = getchamp(id)
            champ = {'name': name, 'id': id}
            blueteam += [champ]
        redteam = []
        for i in range(5):
            id = jogo.red_team.participants[i].champion.id
            name = getchamp(id)
            champ = {'name': name, 'id': id}
            redteam += [champ]
        return {stat: [blueteam] + [redteam]}
    elif stat == 'players':
        ids = []
        for i in range(10):
            ids += [jogo.participants[i].summoner.id]
        return {stat: ids}
    elif stat == 'firstblood':
        return {stat: [int(jogo.blue_team.first_blood), int(jogo.red_team.first_blood)]}
    elif stat == 'skip':
        return


def getPlayerStats(player, region, key, stat):
    '''
    :param player: 'str' ID do jogador que deve ser acessado
    :param region: 'str' Servidor do jogador
    :param key: 'str' Chave de acesso ao API da riot
    :param stat: 'str' Estatística que deseja puxar. Deve estar na lista:
    {'elo'}
    :return: 'vect' Vetor com a estatística do jogador.
    '''
    cass.set_riot_api_key(key)
    summoner = cass.get_summoner(id=player, region=region)
    if stat == 'elo':
        elos = []
        ranks = summoner.ranks
        for key in ranks:
            elos += [str(ranks[key])]
        return elos


keyapi = 'RGAPI-626f2aca-cda8-4793-ad95-7f2db615c471'
cass.set_riot_api_key(keyapi)
stats = {'kda': 1, 'playergold': 2, 'teamgold': 3,
         'playerdamagechamp': 4, 'teamdamagechamp': 5, 'playerdamage': 6, 'teamdamage': 7, 'objetivos': 8, 'win': 9,
         'towers': 10, 'duration': 11, 'champions': 12, 'players': 13, 'firstblood': 14, 'skip': 15}
