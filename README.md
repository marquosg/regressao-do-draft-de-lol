# Regressão do Draft de LoL

Esse projeto começou com a ideia de que a escolha de campeões no jogo League of Legends poderiam ser otimizados durante o chamado draft, de forma que aumente a probabilidade do time observado ganhar, baseado somente nos escolhidos.
Abaixo eu explicarei as etapas realizadas e, ao final, você encontraráá o resultado obtido e algumas conclusões.

# Coleta de dados

A primeira parte, claro, foi a coleta dos dados de partidas. Para isso, utilizei de um pacote chamado cassiopeia, que, utilizando o API da Riot, me possibilitou resgatar dados de partidas de forma progressiva. O método para puxar as partidas foi, partindo de uma lista de usuários de certo servidor(tanto faz qual), resgatar a última partida do histórico de cada um. Além de puxar as informações sobre o jogo, o código também adicionava os outros nove membros para a lista de IDs de jogadores que eu tinha salva no computador. Assim, a ordem de partidas que eram coletadas aumentavam de forma que, se começarmos com uma lista de 10 jogadores(não relacionados, por exemplo, de elos diferentes), coletavamos 10 partidas e adicionavamos 90 mais jogadores pra lista. 
Assim, a fórmula para a quantidade de jogadores na lista é(aproximadamente, sem contar repetidos), sendo i o número da iteração:
<img src="https://latex.codecogs.com/gif.latex?J_{i} = J_{i-1} + 9*J_{i-1}"/>
Em minhas iterações, terminei o código quando cheguei em 53169 amostras. Por duas razões:
* O API da Riot para uso brando tem limite de requests, então demorava anos para conseguir terminar de puxar todos os dados. Minha última iteração demorou três dias. Além disso, a cada 24h era necessário renovar a chave, portanto se eu não estivesse de prontidão na frente do computador, o código estagnava.
* Achei que ia ser o suficiente XD

Os códigos usados para essa tarefa estão na pasta ./Dados.

# Limpeza de dados e análise

Com os dados resgatados, consegui sair da luta pra lidar com o API da Riot e comecei a realmente lidar com os dados. 
A primeira coisa que fiz foi organizar os dados. Você vai encontrar quatro arquivos com dados das partidas dentro da pasta ./Dados/Databases, vou explica-los a seguir.
* "datamatches.json" Esse é o mais puro de todos, que é o que saiu do forninho da coleta de dados. Está em formato de lista de dicionários, onde cada indíce desta é uma partida, com um dicionário contendo alguns dados sobre ela, como campeões, ouros, kdas e etc.
* "dataseparado.json" Esse é parecido com o anterior, porém invertido. Está em formato de dicionário de listas, onde cada chave do dicionário é um atributo, por exemplo número de dragões da partida, e dentro deste há uma lista contendo os dados das 53mil partidas em relação a este número. Além disso, há também uma lista inicial, que separa os dados do blueside e do redside, porém mantendo a ordem de qual partida é pelo índice de cada lista.
* "matches.json" Simples dicionário contendo como chave o ID de cada partida e seu respectivo servidor como valor atribuido.
* "matchescleaned.json" Um dicionário, parecido com o anterior, porém selecionando apenas algumas features que eu julguei relevantes ou desejadas, baseada em análises.
Além disso, você pode encontrar os IDs dos jogadores em players.json, na mesma pasta.


Para analisar os dados, fiz algumas funções, como a realização de uma tabela de correlação e alguns gráficos, que você pode encontrar em ./Dados/Analysis, foi nesta etapa em que selecionei quais features poderiam ser relevantes para o resultado final. Não fiz com todos os 53mil dados, apenas com uma das primeiras levas que eu tive, não sou doido de fazer com tanto dado kkk. Não entrarei muito em detalhe sobre essas observações por achar trivial, fica aí como exercício do leitor a interpretação :)(brinks, podem ter algumas coisas erradas, se tiver algum comentário ou observação, entra em contato comigo!!)

# Análises e resultados com notebooks.

> Só quero deixar um adendo antes de comentar essa parte que esse projeto durou por volta de uns 6/7 meses, então a qualidade do código que você verá vai variar com o período que eu estava fazendo, inclusive a qualidade da validação das ideias, pois durante esse tempo fui codando, estudando e trabalhando com outras coisas e melhorando em geral minhas habilidades(Afinal, essa também era a ideia do projeto).. ;)

Inicialmente, a minha ideia era fazer um modelo que apartir dos campeões da partida, preva as estatísticas, e com essa previsão, use as estatísticas para prever a probabilidade de vitória. O notebook 'redeneural.ipynb' foi utilizado para isso.
A ideia em si já indica problema, estariamos combinando o erro de duas redes para o resultado final, o que se provou um problema. Além disso, apesar das estatísticas preverem facilmente a vitória, chegando a acurácia de 98%, os campeões previam as estatísticas porcamente, chegando a um pouco mais de 57% de acurácia... 

Minha segunda tentativa, após pesquisar um pouco mais, foi utilizar de 
uma regressão logistica para ir direto dos campeões de cada time para o resultado. A forma que eu fiz isso foi, utilizando de features categóricos para indicar a presença de cada campeão em cada time(e o lado que estamos querendo observar a probabilidade de vitória), prever uma label de 0 a 1 em relação a chance de vitória. Mais uma vez, os resultados não foram positivos(porém mais válidos que o anterior), conseguindo uma acurácia de 54% no máximo, o que indica que o modelo apresenta apenas uma chance um pouco maior que um chute.

Finalmente, para fechar o projeto, testei meus dados de campeão num pacote chamado PyCaret, onde esse testa seus dados em vários modelos de aprendizado de máquina para indicar qual é o mais adequado. E vou te falar, sem noticias boas nisso também :(. O pacote indicou que os três melhores modelos para os dados são Regressão Logística, Classificador de Ridge e Análise Discriminante Linear, todos com uma acurácia de 54%(como encontrado anteriormente).

# Conclusões, pensamentos e requisitos

Em relação aos resultados, eu acredito que não é que seja impossível criar um modelo que dado os campeões selecionados nos retorne a probabilidade de vitória, é só que precisa de bem mais dados que isso. Pense comigo. Se cabem 10 campeões numa partida de League of Legends e temos atualmente por volta de 150 campeões possíveis para escolher, então todas as partidas possíveis formam uma Combinação de 150 tomado 10, que nos resulta em 1169554298222310 partidas possíveis. Meus 53k de partidas não abrange nem 0.0001% disso. Talvez com um API profissional(e muito processamento de disco) conseguiria um resultado superior ao encontrado, talvez até satisfatório.

Em relação ao projeto, pra mim foi muito bom, aprendi de tudo um pouco e pude melhorar em todos os quesitos, na parte de buscar dados, pesquisar, modelar, tratar, analisar. Apesar de não ter conseguido obter resultados numéricos positivos, com certeza esse projeto foi um sucesso para meu desenvolvimento pessoal, talvez daqui uns anos volte a visita-lo. :)

Os requisitos estão no requeriments.txt o_O



